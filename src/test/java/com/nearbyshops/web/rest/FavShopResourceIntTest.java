package com.nearbyshops.web.rest;

import com.nearbyshops.HiddenfoundersApp;

import com.nearbyshops.domain.FavShop;
import com.nearbyshops.repository.FavShopRepository;
import com.nearbyshops.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static com.nearbyshops.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FavShopResource REST controller.
 *
 * @see FavShopResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HiddenfoundersApp.class)
public class FavShopResourceIntTest {

    private static final Integer DEFAULT_ID_USER = 1;
    private static final Integer UPDATED_ID_USER = 2;

    private static final Integer DEFAULT_ID_SHOP = 1;
    private static final Integer UPDATED_ID_SHOP = 2;

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final Instant DEFAULT_DISLIKE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DISLIKE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private FavShopRepository favShopRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restFavShopMockMvc;

    private FavShop favShop;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FavShopResource favShopResource = new FavShopResource(favShopRepository);
        this.restFavShopMockMvc = MockMvcBuilders.standaloneSetup(favShopResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FavShop createEntity() {
        FavShop favShop = new FavShop()
            .idUser(DEFAULT_ID_USER)
            .idShop(DEFAULT_ID_SHOP)
            .status(DEFAULT_STATUS)
            .dislikeTime(DEFAULT_DISLIKE_TIME);
        return favShop;
    }

    @Before
    public void initTest() {
        favShopRepository.deleteAll();
        favShop = createEntity();
    }

    @Test
    public void createFavShop() throws Exception {
        int databaseSizeBeforeCreate = favShopRepository.findAll().size();

        // Create the FavShop
        restFavShopMockMvc.perform(post("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favShop)))
            .andExpect(status().isCreated());

        // Validate the FavShop in the database
        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeCreate + 1);
        FavShop testFavShop = favShopList.get(favShopList.size() - 1);
        assertThat(testFavShop.getIdUser()).isEqualTo(DEFAULT_ID_USER);
        assertThat(testFavShop.getIdShop()).isEqualTo(DEFAULT_ID_SHOP);
        assertThat(testFavShop.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testFavShop.getDislikeTime()).isEqualTo(DEFAULT_DISLIKE_TIME);
    }

    @Test
    public void createFavShopWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = favShopRepository.findAll().size();

        // Create the FavShop with an existing ID
        favShop.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restFavShopMockMvc.perform(post("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favShop)))
            .andExpect(status().isBadRequest());

        // Validate the FavShop in the database
        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkIdUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = favShopRepository.findAll().size();
        // set the field null
        favShop.setIdUser(null);

        // Create the FavShop, which fails.

        restFavShopMockMvc.perform(post("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favShop)))
            .andExpect(status().isBadRequest());

        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkIdShopIsRequired() throws Exception {
        int databaseSizeBeforeTest = favShopRepository.findAll().size();
        // set the field null
        favShop.setIdShop(null);

        // Create the FavShop, which fails.

        restFavShopMockMvc.perform(post("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favShop)))
            .andExpect(status().isBadRequest());

        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllFavShops() throws Exception {
        // Initialize the database
        favShopRepository.save(favShop);

        // Get all the favShopList
        restFavShopMockMvc.perform(get("/api/fav-shops?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(favShop.getId())))
            .andExpect(jsonPath("$.[*].idUser").value(hasItem(DEFAULT_ID_USER)))
            .andExpect(jsonPath("$.[*].idShop").value(hasItem(DEFAULT_ID_SHOP)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].dislikeTime").value(hasItem(DEFAULT_DISLIKE_TIME.toString())));
    }
    
    @Test
    public void getFavShop() throws Exception {
        // Initialize the database
        favShopRepository.save(favShop);

        // Get the favShop
        restFavShopMockMvc.perform(get("/api/fav-shops/{id}", favShop.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(favShop.getId()))
            .andExpect(jsonPath("$.idUser").value(DEFAULT_ID_USER))
            .andExpect(jsonPath("$.idShop").value(DEFAULT_ID_SHOP))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.dislikeTime").value(DEFAULT_DISLIKE_TIME.toString()));
    }

    @Test
    public void getNonExistingFavShop() throws Exception {
        // Get the favShop
        restFavShopMockMvc.perform(get("/api/fav-shops/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateFavShop() throws Exception {
        // Initialize the database
        favShopRepository.save(favShop);

        int databaseSizeBeforeUpdate = favShopRepository.findAll().size();

        // Update the favShop
        FavShop updatedFavShop = favShopRepository.findById(favShop.getId()).get();
        updatedFavShop
            .idUser(UPDATED_ID_USER)
            .idShop(UPDATED_ID_SHOP)
            .status(UPDATED_STATUS)
            .dislikeTime(UPDATED_DISLIKE_TIME);

        restFavShopMockMvc.perform(put("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFavShop)))
            .andExpect(status().isOk());

        // Validate the FavShop in the database
        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeUpdate);
        FavShop testFavShop = favShopList.get(favShopList.size() - 1);
        assertThat(testFavShop.getIdUser()).isEqualTo(UPDATED_ID_USER);
        assertThat(testFavShop.getIdShop()).isEqualTo(UPDATED_ID_SHOP);
        assertThat(testFavShop.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testFavShop.getDislikeTime()).isEqualTo(UPDATED_DISLIKE_TIME);
    }

    @Test
    public void updateNonExistingFavShop() throws Exception {
        int databaseSizeBeforeUpdate = favShopRepository.findAll().size();

        // Create the FavShop

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFavShopMockMvc.perform(put("/api/fav-shops")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favShop)))
            .andExpect(status().isBadRequest());

        // Validate the FavShop in the database
        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteFavShop() throws Exception {
        // Initialize the database
        favShopRepository.save(favShop);

        int databaseSizeBeforeDelete = favShopRepository.findAll().size();

        // Get the favShop
        restFavShopMockMvc.perform(delete("/api/fav-shops/{id}", favShop.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FavShop> favShopList = favShopRepository.findAll();
        assertThat(favShopList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FavShop.class);
        FavShop favShop1 = new FavShop();
        favShop1.setId("id1");
        FavShop favShop2 = new FavShop();
        favShop2.setId(favShop1.getId());
        assertThat(favShop1).isEqualTo(favShop2);
        favShop2.setId("id2");
        assertThat(favShop1).isNotEqualTo(favShop2);
        favShop1.setId(null);
        assertThat(favShop1).isNotEqualTo(favShop2);
    }
}
