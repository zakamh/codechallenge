import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './fav-shop.reducer';
import { IFavShop } from 'app/shared/model/fav-shop.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFavShopUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IFavShopUpdateState {
  isNew: boolean;
}

export class FavShopUpdate extends React.Component<IFavShopUpdateProps, IFavShopUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.dislikeTime = new Date(values.dislikeTime);

    if (errors.length === 0) {
      const { favShopEntity } = this.props;
      const entity = {
        ...favShopEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/fav-shop');
  };

  render() {
    const { favShopEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="hiddenfoundersApp.favShop.home.createOrEditLabel">Create or edit a FavShop</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : favShopEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">ID</Label>
                    <AvInput id="fav-shop-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="idUserLabel" for="idUser">
                    Id User
                  </Label>
                  <AvField
                    id="fav-shop-idUser"
                    type="string"
                    className="form-control"
                    name="idUser"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="idShopLabel" for="idShop">
                    Id Shop
                  </Label>
                  <AvField
                    id="fav-shop-idShop"
                    type="string"
                    className="form-control"
                    name="idShop"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" check>
                    <AvInput id="fav-shop-status" type="checkbox" className="form-control" name="status" />
                    Status
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="dislikeTimeLabel" for="dislikeTime">
                    Dislike Time
                  </Label>
                  <AvInput
                    id="fav-shop-dislikeTime"
                    type="datetime-local"
                    className="form-control"
                    name="dislikeTime"
                    value={isNew ? null : convertDateTimeFromServer(this.props.favShopEntity.dislikeTime)}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/fav-shop" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  favShopEntity: storeState.favShop.entity,
  loading: storeState.favShop.loading,
  updating: storeState.favShop.updating,
  updateSuccess: storeState.favShop.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavShopUpdate);
