import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './fav-shop.reducer';
import { IFavShop } from 'app/shared/model/fav-shop.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFavShopDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class FavShopDetail extends React.Component<IFavShopDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { favShopEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            FavShop [<b>{favShopEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="idUser">Id User</span>
            </dt>
            <dd>{favShopEntity.idUser}</dd>
            <dt>
              <span id="idShop">Id Shop</span>
            </dt>
            <dd>{favShopEntity.idShop}</dd>
            <dt>
              <span id="status">Status</span>
            </dt>
            <dd>{favShopEntity.status ? 'true' : 'false'}</dd>
            <dt>
              <span id="dislikeTime">Dislike Time</span>
            </dt>
            <dd>
              <TextFormat value={favShopEntity.dislikeTime} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/fav-shop" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/fav-shop/${favShopEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ favShop }: IRootState) => ({
  favShopEntity: favShop.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavShopDetail);
