import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './fav-shop.reducer';
import { IFavShop } from 'app/shared/model/fav-shop.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFavShopProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class FavShop extends React.Component<IFavShopProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { favShopList, match } = this.props;
    return (
      <div>
        <h2 id="fav-shop-heading">
          Fav Shops
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Fav Shop
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Id User</th>
                <th>Id Shop</th>
                <th>Status</th>
                <th>Dislike Time</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {favShopList.map((favShop, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${favShop.id}`} color="link" size="sm">
                      {favShop.id}
                    </Button>
                  </td>
                  <td>{favShop.idUser}</td>
                  <td>{favShop.idShop}</td>
                  <td>{favShop.status ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={favShop.dislikeTime} format={APP_DATE_FORMAT} />
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${favShop.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${favShop.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${favShop.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ favShop }: IRootState) => ({
  favShopList: favShop.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavShop);
