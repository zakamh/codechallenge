import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFavShop, defaultValue } from 'app/shared/model/fav-shop.model';

export const ACTION_TYPES = {
  FETCH_FAVSHOP_LIST: 'favShop/FETCH_FAVSHOP_LIST',
  FETCH_FAVSHOP: 'favShop/FETCH_FAVSHOP',
  CREATE_FAVSHOP: 'favShop/CREATE_FAVSHOP',
  UPDATE_FAVSHOP: 'favShop/UPDATE_FAVSHOP',
  DELETE_FAVSHOP: 'favShop/DELETE_FAVSHOP',
  RESET: 'favShop/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFavShop>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type FavShopState = Readonly<typeof initialState>;

// Reducer

export default (state: FavShopState = initialState, action): FavShopState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FAVSHOP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FAVSHOP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FAVSHOP):
    case REQUEST(ACTION_TYPES.UPDATE_FAVSHOP):
    case REQUEST(ACTION_TYPES.DELETE_FAVSHOP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FAVSHOP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FAVSHOP):
    case FAILURE(ACTION_TYPES.CREATE_FAVSHOP):
    case FAILURE(ACTION_TYPES.UPDATE_FAVSHOP):
    case FAILURE(ACTION_TYPES.DELETE_FAVSHOP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FAVSHOP_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_FAVSHOP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FAVSHOP):
    case SUCCESS(ACTION_TYPES.UPDATE_FAVSHOP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FAVSHOP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/fav-shops';

// Actions

export const getEntities: ICrudGetAllAction<IFavShop> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_FAVSHOP_LIST,
  payload: axios.get<IFavShop>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IFavShop> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FAVSHOP,
    payload: axios.get<IFavShop>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IFavShop> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FAVSHOP,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFavShop> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FAVSHOP,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFavShop> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FAVSHOP,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
