import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FavShop from './fav-shop';
import FavShopDetail from './fav-shop-detail';
import FavShopUpdate from './fav-shop-update';
import FavShopDeleteDialog from './fav-shop-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FavShopUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FavShopUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FavShopDetail} />
      <ErrorBoundaryRoute path={match.url} component={FavShop} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FavShopDeleteDialog} />
  </>
);

export default Routes;
