import React from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from '../header-components';

export const EntitiesMenu = props => (
  // tslint:disable-next-line:jsx-self-close
  <Nav name="Entities" id="entity-menu">
    <NavItem>
      <NavLink tag={Link} to="/entity/shop" className="d-flex align-items-center">
        <FontAwesomeIcon icon="asterisk" />
        &nbsp;Shop
      </NavLink>
    </NavItem>
    <NavItem>
      <NavLink tag={Link} to="/entity/fav-shop" className="d-flex align-items-center">
        <FontAwesomeIcon icon="asterisk" />
        &nbsp;Fav Shop
      </NavLink>
    </NavItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    {/*<NavDropdown icon="th-list" name="Entities" id="entity-menu">
              <DropdownItem tag={Link} to="/entity/shop">
                <FontAwesomeIcon icon="asterisk" />
                &nbsp;Shop
              </DropdownItem>
              <DropdownItem tag={Link} to="/entity/fav-shop">
                <FontAwesomeIcon icon="asterisk" />
                &nbsp;Fav Shop
              </DropdownItem>
            </NavDropdown>*/}
  </Nav>
);
