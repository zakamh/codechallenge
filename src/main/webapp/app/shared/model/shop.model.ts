export interface IShop {
  id?: string;
  idShop?: number;
  name?: string;
  latitude?: number;
  longitude?: number;
  imageContentType?: string;
  image?: any;
}

export const defaultValue: Readonly<IShop> = {};
