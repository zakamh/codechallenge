import { Moment } from 'moment';

export interface IFavShop {
  id?: string;
  idUser?: number;
  idShop?: number;
  status?: boolean;
  dislikeTime?: Moment;
}

export const defaultValue: Readonly<IFavShop> = {
  status: false
};
