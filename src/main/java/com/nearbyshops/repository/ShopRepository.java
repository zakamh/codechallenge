package com.nearbyshops.repository;

import com.nearbyshops.domain.Shop;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Shop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopRepository extends MongoRepository<Shop, String> {

}
