package com.nearbyshops.repository;

import com.nearbyshops.domain.FavShop;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the FavShop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FavShopRepository extends MongoRepository<FavShop, String> {

}
