package com.nearbyshops.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A FavShop.
 */
@Document(collection = "fav_shop")
public class FavShop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("id_user")
    private Integer idUser;

    @NotNull
    @Field("id_shop")
    private Integer idShop;

    @Field("status")
    private Boolean status;

    @Field("dislike_time")
    private Instant dislikeTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public FavShop idUser(Integer idUser) {
        this.idUser = idUser;
        return this;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdShop() {
        return idShop;
    }

    public FavShop idShop(Integer idShop) {
        this.idShop = idShop;
        return this;
    }

    public void setIdShop(Integer idShop) {
        this.idShop = idShop;
    }

    public Boolean isStatus() {
        return status;
    }

    public FavShop status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Instant getDislikeTime() {
        return dislikeTime;
    }

    public FavShop dislikeTime(Instant dislikeTime) {
        this.dislikeTime = dislikeTime;
        return this;
    }

    public void setDislikeTime(Instant dislikeTime) {
        this.dislikeTime = dislikeTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FavShop favShop = (FavShop) o;
        if (favShop.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), favShop.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FavShop{" +
            "id=" + getId() +
            ", idUser=" + getIdUser() +
            ", idShop=" + getIdShop() +
            ", status='" + isStatus() + "'" +
            ", dislikeTime='" + getDislikeTime() + "'" +
            "}";
    }
}
