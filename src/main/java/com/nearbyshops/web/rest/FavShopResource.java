package com.nearbyshops.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nearbyshops.domain.FavShop;
import com.nearbyshops.repository.FavShopRepository;
import com.nearbyshops.web.rest.errors.BadRequestAlertException;
import com.nearbyshops.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FavShop.
 */
@RestController
@RequestMapping("/api")
public class FavShopResource {

    private final Logger log = LoggerFactory.getLogger(FavShopResource.class);

    private static final String ENTITY_NAME = "favShop";

    private FavShopRepository favShopRepository;

    public FavShopResource(FavShopRepository favShopRepository) {
        this.favShopRepository = favShopRepository;
    }

    /**
     * POST  /fav-shops : Create a new favShop.
     *
     * @param favShop the favShop to create
     * @return the ResponseEntity with status 201 (Created) and with body the new favShop, or with status 400 (Bad Request) if the favShop has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/fav-shops")
    @Timed
    public ResponseEntity<FavShop> createFavShop(@Valid @RequestBody FavShop favShop) throws URISyntaxException {
        log.debug("REST request to save FavShop : {}", favShop);
        if (favShop.getId() != null) {
            throw new BadRequestAlertException("A new favShop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FavShop result = favShopRepository.save(favShop);
        return ResponseEntity.created(new URI("/api/fav-shops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fav-shops : Updates an existing favShop.
     *
     * @param favShop the favShop to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated favShop,
     * or with status 400 (Bad Request) if the favShop is not valid,
     * or with status 500 (Internal Server Error) if the favShop couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/fav-shops")
    @Timed
    public ResponseEntity<FavShop> updateFavShop(@Valid @RequestBody FavShop favShop) throws URISyntaxException {
        log.debug("REST request to update FavShop : {}", favShop);
        if (favShop.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FavShop result = favShopRepository.save(favShop);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, favShop.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fav-shops : get all the favShops.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of favShops in body
     */
    @GetMapping("/fav-shops")
    @Timed
    public List<FavShop> getAllFavShops() {
        log.debug("REST request to get all FavShops");
        return favShopRepository.findAll();
    }

    /**
     * GET  /fav-shops/:id : get the "id" favShop.
     *
     * @param id the id of the favShop to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the favShop, or with status 404 (Not Found)
     */
    @GetMapping("/fav-shops/{id}")
    @Timed
    public ResponseEntity<FavShop> getFavShop(@PathVariable String id) {
        log.debug("REST request to get FavShop : {}", id);
        Optional<FavShop> favShop = favShopRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(favShop);
    }

    /**
     * DELETE  /fav-shops/:id : delete the "id" favShop.
     *
     * @param id the id of the favShop to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fav-shops/{id}")
    @Timed
    public ResponseEntity<Void> deleteFavShop(@PathVariable String id) {
        log.debug("REST request to delete FavShop : {}", id);

        favShopRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
