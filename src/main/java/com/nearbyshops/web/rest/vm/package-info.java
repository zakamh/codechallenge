/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nearbyshops.web.rest.vm;
